package com.experis.se.mbefj.assignment3.controllers;

import com.experis.se.mbefj.assignment3.mappers.CharacterMapper;
import com.experis.se.mbefj.assignment3.mappers.FranchiseMapper;
import com.experis.se.mbefj.assignment3.mappers.MovieMapper;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterDTO;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterInMovieDTO;
import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.franchise.dtos.FranchiseDTO;
import com.experis.se.mbefj.assignment3.model.franchise.dtos.FranchiseUpdateDTO;
import com.experis.se.mbefj.assignment3.model.movie.dtos.MovieDTO;
import com.experis.se.mbefj.assignment3.services.character.CharacterService;
import com.experis.se.mbefj.assignment3.services.franchise.FranchiseService;
import com.experis.se.mbefj.assignment3.services.movie.MovieService;
import com.experis.se.mbefj.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper,
                               MovieService movieService, MovieMapper movieMapper, CharacterService characterService,
                               CharacterMapper characterMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all franchises.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    })
    })
    @GetMapping
    public ResponseEntity findAll() {
        var franchises = franchiseService.findAll();
        return ResponseEntity.ok(franchiseMapper.franchiseToFranchiseDto(franchises));
    }

    @Operation(summary = "Get a franchise by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping({"{id}"})
    public ResponseEntity getById(@PathVariable int id) {
        var franchise = franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id));
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "Get all movies in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "No franchise exists with the supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping({"{id}/movies"})
    public ResponseEntity getMoviesInFranchise(@PathVariable int id) {
        var franchise = franchiseService.findById(id);
        var movies = movieService.findMoviesByFranchise(franchise);
        return ResponseEntity.ok(movieMapper.movieToMovieDTO(movies));
    }

    @Operation(summary = "Update which movies belong to the franchise specified by the id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movies were updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movies in the franchise were not updated because the request references movie ids that do not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @PutMapping({"{id}/movies"})
    public ResponseEntity updateMoviesInFranchise(@PathVariable int id, @RequestBody List<Integer> movies) {
        movieService.updateMoviesInFranchise(id, movies);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all characters in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterInMovieDTO.class)))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "No franchise exists with the supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping({"{id}/characters"})
    public ResponseEntity getCharactersInFranchise(@PathVariable int id) {
        var franchise = franchiseService.findById(id);
        var characters = characterService.findCharactersByFranchise(franchise);
        return ResponseEntity.ok(characterMapper.charToCharInMovieDTO(characters));
    }

    @Operation(summary = "Add a new franchise to the database.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Franchise was created",
                    content = {@Content(mediaType = "application/json")}
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Franchise franchise) {
        var frToAdd = franchiseService.add(franchise);
        var location = URI.create("franchises/" + frToAdd.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a franchise's data. ")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Franchise was updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Franchise id in body does not match id in path.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            ),
    })
    @PutMapping({"{id}"})
    public ResponseEntity update(@RequestBody FranchiseUpdateDTO franchiseUpdateDTO, @PathVariable int id) {
        if (id != franchiseUpdateDTO.getId()) {
            return ResponseEntity.badRequest().build();
        }
        franchiseService.update(franchiseMapper.franchiseUpdateDtoToFranchise(franchiseUpdateDTO));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a franchise by id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Franchise was deleted",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "The franchise with the specified id did not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id){
        var franchise = franchiseService.findById(id);
        movieService.deleteFranchiseFromMovies(franchise);
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
