package com.experis.se.mbefj.assignment3.mappers;

import com.experis.se.mbefj.assignment3.model.character.Character;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterDTO;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterInMovieDTO;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterUpdateDTO;
import com.experis.se.mbefj.assignment3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;

    public abstract CharacterInMovieDTO charToCharInMovieDTO(Character character);

    public abstract Collection<CharacterInMovieDTO> charToCharInMovieDTO(Collection<Character> characters);

    @Mapping(target = "movieIds", source = "movies", qualifiedByName = "moviesToMovieIds")
    public abstract CharacterDTO charToCharDTO(Character character);

    public abstract Collection<CharacterDTO> charToCharDTO(Collection<Character> characters);

    @Named("moviesToMovieIds")
    List<Integer> moviesToMovieIds(Set<Movie> movies) {
        if (movies == null) {
            return List.of();
        }

        return movies.stream()
                .map(Movie::getId)
                .toList();
    }

    public abstract Character charInMovieDTOtoChar(CharacterInMovieDTO characterInMovieDTO);

    public abstract Collection<Character> charInMovieDTOtoChar(Collection<CharacterInMovieDTO> characterInMovieDTO);

    @Mapping(target = "movies", source = "movieIds", qualifiedByName = "movieIdsToMovie")
    public abstract Character charDTOtoChar(CharacterDTO characterDTO);

    public abstract Collection<Character> charDTOtoChar(Collection<CharacterDTO> characterDTO);

    @Named("movieIdsToMovie")
    Set<Movie> movieIdsToMovie(List<Integer> movieIds) {
        return movieIds.stream()
                .map(movieService::findById)
                .collect(Collectors.toSet());
    }

    public abstract Character charUpdateDTOtoChar(CharacterUpdateDTO characterUpdateDTO);

    public abstract Collection<Character> charUpdateDTOtoChar(Collection<CharacterUpdateDTO> characterUpdateDTO);
}
