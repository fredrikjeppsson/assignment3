package com.experis.se.mbefj.assignment3.controllers;

import com.experis.se.mbefj.assignment3.mappers.CharacterMapper;
import com.experis.se.mbefj.assignment3.mappers.GenreMapper;
import com.experis.se.mbefj.assignment3.mappers.MovieMapper;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterInMovieDTO;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreNewDTO;
import com.experis.se.mbefj.assignment3.model.movie.dtos.MovieDTO;
import com.experis.se.mbefj.assignment3.model.movie.dtos.MovieNewDTO;
import com.experis.se.mbefj.assignment3.services.character.CharacterService;
import com.experis.se.mbefj.assignment3.services.genre.GenreService;
import com.experis.se.mbefj.assignment3.services.movie.MovieService;
import com.experis.se.mbefj.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("api/v1/movies")
public class MovieController {
    private final MovieMapper movieMapper;
    private final MovieService movieService;
    private final CharacterMapper characterMapper;
    private final CharacterService characterService;
    private final GenreService genreService;
    private final GenreMapper genreMapper;

    public MovieController(MovieMapper movieMapper, MovieService movieService, CharacterMapper characterMapper, CharacterService characterService, GenreService genreService, GenreMapper genreMapper) {
        this.movieMapper = movieMapper;
        this.movieService = movieService;
        this.characterMapper = characterMapper;
        this.characterService = characterService;
        this.genreService = genreService;
        this.genreMapper = genreMapper;
    }

    @Operation(summary = "Get all movies.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    })
    })
    @GetMapping
    public ResponseEntity findAll() {
        var movies = movieService.findAll();
        return ResponseEntity.ok(movieMapper.movieToMovieDTO(movies));
    }

    @Operation(summary = "Get a movie by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        var movie = movieService.findById(id);
        return ResponseEntity.ok(movieMapper.movieToMovieDTO(movie));
    }

    @Operation(summary = "Get all characters in a movie.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterInMovieDTO.class)))
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "A movie with the supplied id does not exist.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}/characters")
    public ResponseEntity findCharactersInMovie(@PathVariable int id) {
        var movie = movieService.findById(id);
        var characters = characterService.findByMovie(movie);
        return ResponseEntity.ok(characterMapper.charToCharInMovieDTO(characters));
    }

    @Operation(summary = "Get a movie's genre(s).")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = GenreNewDTO.class)))
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "A movie with the supplied id does not exist.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}/genres")
    public ResponseEntity findGenresInMovie(@PathVariable int id) {
        var movie = movieService.findById(id);
        var genres = genreService.findByMovie(movie);
        return ResponseEntity.ok(genreMapper.genreToGenreNewDTO(genres));
    }

    @Operation(summary = "Add a new movie to the database.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Movie was created",
                    content = {@Content(mediaType = "application/json")}
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody MovieNewDTO movieNewDTO) {
        var movie = movieMapper.movieNewDTOToMovie(movieNewDTO);
        var addedMovie = movieService.add(movie);
        URI uri = URI.create("movies/" + addedMovie.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a movie's data. ")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie was updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Movie id in body does not match id in path.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie was not updated because the request references genre ids or movies ids that do not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MovieDTO movieDTO, @PathVariable int id) {
        if (movieDTO.getId() != id)
            return ResponseEntity.badRequest().build();

        var movie = movieMapper.movieDTOtoMovie(movieDTO);
        movieService.update(movie);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update which characters are in a movie.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie characters were updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie characters were not updated because the request references a character that does not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @PutMapping("/{id}/characters")
    public ResponseEntity updateCharacters(@RequestBody List<Integer> movieCharactersIds, @PathVariable int id) {
        movieService.updateCharacters(id, movieCharactersIds);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update which genre(s) a movie has.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Genres were updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie genres were not updated because the request references genres that do not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @PutMapping("/{id}/genres")
    public ResponseEntity updateGenres(@RequestBody List<Integer> genreIds, @PathVariable int id) {
        movieService.updateGenres(id, genreIds);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a movie by id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie was deleted",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "The movie with the specified id did not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
