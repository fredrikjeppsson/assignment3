package com.experis.se.mbefj.assignment3.model.genre.dtos;

public class GenreNewDTO {
    private String genreName;

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
