package com.experis.se.mbefj.assignment3.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FranchiseException extends RuntimeException{
    public FranchiseException(String message){ super(message);}
}
