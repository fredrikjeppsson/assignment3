package com.experis.se.mbefj.assignment3.services.genre;

import com.experis.se.mbefj.assignment3.model.genre.Genre;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.services.CrudService;

import java.util.Set;

public interface GenreService extends CrudService<Genre, Integer> {
    Set<Genre> findByMovie(Movie movie);
}
