package com.experis.se.mbefj.assignment3.services.genre;

import com.experis.se.mbefj.assignment3.model.genre.Genre;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.repositories.GenreRepository;
import com.experis.se.mbefj.assignment3.services.exceptions.GenreException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class GenreServiceImpl implements GenreService {
    private final GenreRepository genreRepository;

    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public Genre findById(Integer id) {
        return genreRepository.findById(id).orElseThrow(() -> new GenreException("Could not find genre with id: " + id));
    }

    @Override
    public Collection<Genre> findAll() {
        return genreRepository.findAll();
    }

    @Override
    public Genre add(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    public Genre update(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    public void deleteById(Integer id) {
        try {
            genreRepository.deleteById(id);
        } catch (Exception e) {
            throw new GenreException("Could not delete genre with id: " + id);
        }

    }

    @Override
    public Set<Genre> findByMovie(Movie movie) {
        return movie.getGenres();
    }
}
