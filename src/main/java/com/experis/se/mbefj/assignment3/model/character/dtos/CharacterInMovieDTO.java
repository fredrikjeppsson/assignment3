package com.experis.se.mbefj.assignment3.model.character.dtos;

public class CharacterInMovieDTO {
    private String fullName;
    private String charAlias;
    private String gender;
    private String pictureUrl;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCharAlias() {
        return charAlias;
    }

    public void setCharAlias(String charAlias) {
        this.charAlias = charAlias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
