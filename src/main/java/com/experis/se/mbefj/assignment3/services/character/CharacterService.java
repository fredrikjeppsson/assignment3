package com.experis.se.mbefj.assignment3.services.character;

import com.experis.se.mbefj.assignment3.model.character.Character;
import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.services.CrudService;

import java.util.List;

public interface CharacterService extends CrudService<Character, Integer> {
    List<Character> findByMovie(Movie movie);
    List<Character> findCharactersByFranchise(Franchise franchise);
}
