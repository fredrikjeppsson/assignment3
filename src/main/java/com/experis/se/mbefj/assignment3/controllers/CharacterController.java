package com.experis.se.mbefj.assignment3.controllers;

import com.experis.se.mbefj.assignment3.mappers.CharacterMapper;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterDTO;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterInMovieDTO;
import com.experis.se.mbefj.assignment3.model.character.dtos.CharacterUpdateDTO;
import com.experis.se.mbefj.assignment3.model.movie.dtos.MovieDTO;
import com.experis.se.mbefj.assignment3.services.character.CharacterService;
import com.experis.se.mbefj.assignment3.services.movie.MovieService;
import com.experis.se.mbefj.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final MovieService movieService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, MovieService movieService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.movieService = movieService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all characters.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    })
    })
    @GetMapping
    public ResponseEntity findAll() {
        var characters = characterService.findAll();
        var characterDTOs = characterMapper.charToCharDTO(characters);
        return ResponseEntity.ok(characterDTOs);
    }

    @Operation(summary = "Get a character by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        var movie = characterService.findById(id);
        return ResponseEntity.ok(characterMapper.charToCharDTO(movie));
    }

    @Operation(summary = "Add a new character to the database.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Character was created",
                    content = {@Content(mediaType = "application/json")}
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody CharacterInMovieDTO characterInMovieDTO) {
        var character = characterMapper.charInMovieDTOtoChar(characterInMovieDTO);
        var addedCharacter = characterService.add(character);
        URI uri = URI.create("movies/" + addedCharacter.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a character's data. ")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Character was updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Character id in body does not match id in path.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody CharacterUpdateDTO characterUpdateDTO, @PathVariable int id) {
        if (characterUpdateDTO.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        var character = characterMapper.charUpdateDTOtoChar(characterUpdateDTO);
        characterService.update(character);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a character by id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Character was deleted",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "The character with the specified id did not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        var character = characterService.findById(id);
        movieService.deleteCharacterFromMovies(character);
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
