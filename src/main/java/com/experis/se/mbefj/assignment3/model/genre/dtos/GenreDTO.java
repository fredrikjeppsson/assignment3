package com.experis.se.mbefj.assignment3.model.genre.dtos;

import java.util.List;

public class GenreDTO {
    private int id;
    private String genreName;
    private List<Integer> movieIds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public List<Integer> getMovieIds() {
        return movieIds;
    }

    public void setMovieIds(List<Integer> movieIds) {
        this.movieIds = movieIds;
    }
}
