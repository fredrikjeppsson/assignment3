package com.experis.se.mbefj.assignment3.services.movie;


import com.experis.se.mbefj.assignment3.model.character.Character;
import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.genre.Genre;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.repositories.MovieRepository;
import com.experis.se.mbefj.assignment3.services.character.CharacterService;
import com.experis.se.mbefj.assignment3.services.exceptions.MovieException;
import com.experis.se.mbefj.assignment3.services.franchise.FranchiseService;
import com.experis.se.mbefj.assignment3.services.genre.GenreService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final CharacterService characterService;
    private final FranchiseService franchiseService;
    private final GenreService genreService;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterService characterService, FranchiseService franchiseService, GenreService genreService) {
        this.movieRepository = movieRepository;
        this.characterService = characterService;
        this.franchiseService = franchiseService;
        this.genreService = genreService;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new MovieException("Could not find movie with id: " + id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public Movie update(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public void deleteById(Integer id) {
        try {
            movieRepository.deleteById(id);
        } catch (Exception e) {
            throw new MovieException("Could not delete movie with id: " + id);
        }
    }

    @Override
    public void updateCharacters(int id, List<Integer> movieCharactersIds) {
        var movie = findById(id);
        var characters = movieCharactersIds.stream()
                .map(characterService::findById)
                .collect(Collectors.toSet());
        movie.setCharacters(characters);
        movieRepository.save(movie);
    }

    @Override
    public List<Movie> findMoviesByFranchise(Franchise franchise) {
        return findAll().stream()
                .filter(movie -> movie.getFranchise().getId() == franchise.getId())
                .toList();
    }

    @Override
    public void updateMoviesInFranchise(int id, List<Integer> movieIds) {
        var franchise = franchiseService.findById(id);
        var movies = movieIds.stream()
                .map(this::findById)
                .toList();
        // This is done in two steps such that no change is made if one or more of the
        // movie ids do not exist. Otherwise, we'll get a partially completed
        // operation if the invalid id is found in after the first position.
        movies.forEach(movie -> {
            movie.setFranchise(franchise);
            movieRepository.save(movie);
        });
    }

    @Override
    public void deleteCharacterFromMovies(Character character) {
        var movies = findAll();
        movies.forEach(
                movie -> {
                    var characters = movie.getCharacters();
                    characters.remove(character);
                    update(movie);
                }
        );
    }

    @Override
    public void deleteGenreFromMovies(Genre genre) {
        var movies = findAll();
        movies.forEach(
                movie -> {
                    var genres = movie.getGenres();
                    genres.remove(genre);
                    update(movie);
                }
        );
    }

    @Override
    public void deleteFranchiseFromMovies(Franchise franchise){
        var movies = findMoviesByFranchise(franchise);
        movies.forEach(
                movie -> {
                    movie.setFranchise(null);
                }
        );
    }

    @Override
    public void updateGenres(int id, List<Integer> genreIds) {
        var movie = findById(id);
        var genres = genreIds.stream()
                .map(genreService::findById)
                .collect(Collectors.toSet());
        movie.setGenres(genres);
        movieRepository.save(movie);
    }
}
