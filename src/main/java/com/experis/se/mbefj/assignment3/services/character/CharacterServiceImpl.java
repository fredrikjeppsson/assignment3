package com.experis.se.mbefj.assignment3.services.character;

import com.experis.se.mbefj.assignment3.model.character.Character;
import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.repositories.CharacterRepository;
import com.experis.se.mbefj.assignment3.services.exceptions.CharacterException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(
                () -> new CharacterException("Could not find character with id: " + id)
        );
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character character) {
        return characterRepository.save(character);
    }

    @Override
    public Character update(Character character) {
        return characterRepository.save(character);
    }

    @Override
    public void deleteById(Integer id) {
        try {
            characterRepository.deleteById(id);
        } catch (Exception e) {
            throw new CharacterException("Could not delete character with id: " + id);
        }
    }

    @Override
    public List<Character> findByMovie(Movie movie) {
        return movie.getCharacters().stream().toList();
    }

    @Override
    public List<Character> findCharactersByFranchise(Franchise franchise) {
        return findAll().stream()
                .filter(character -> characterInFranchise(character, franchise.getId()))
                .toList();
    }

    private boolean characterInFranchise(Character character, int franchiseId) {
        return character.getMovies().stream()
                .map(Movie::getFranchise)
                .map(Franchise::getId)
                .collect(Collectors.toSet())
                .contains(franchiseId);
    }
}
