package com.experis.se.mbefj.assignment3.mappers;

import com.experis.se.mbefj.assignment3.model.genre.Genre;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreDTO;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreNewDTO;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreUpdateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class GenreMapper {
    @Mapping(target = "movieIds", source = "movies", qualifiedByName = "moviesToId")
    public abstract GenreDTO genreToGenreDTO(Genre genre);

    public abstract Collection<GenreDTO> genreDTOs(Collection<Genre> genre);

    @Named("moviesToId")
    public List<Integer> moviesToId(Set<Movie> movies) {
        if (movies == null) {
            return List.of();
        }
        return movies.stream().map(Movie::getId).toList();
    }

    public abstract Genre genreNewDTOtoGenre(GenreNewDTO genreNewDTO);


    public abstract Genre genreToGenreDTO(GenreDTO genreDTO);
    public abstract Collection<Genre> genreToGenreDTO(Collection<GenreDTO> genreDTO);

    public abstract Genre genreUpdateDTOtoGenre(GenreUpdateDTO genreUpdateDTO);
    public abstract Collection<Genre> genreUpdateDTOtoGenre(Collection<GenreUpdateDTO> genreUpdateDTO);

    public abstract GenreNewDTO genreToGenreNewDTO(Genre genre);
    public abstract Collection<GenreNewDTO> genreToGenreNewDTO(Collection<Genre> genre);
}
