package com.experis.se.mbefj.assignment3.repositories;

import com.experis.se.mbefj.assignment3.model.genre.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {
}
