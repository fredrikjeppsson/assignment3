package com.experis.se.mbefj.assignment3.controllers;


import com.experis.se.mbefj.assignment3.mappers.GenreMapper;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreDTO;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreNewDTO;
import com.experis.se.mbefj.assignment3.model.genre.dtos.GenreUpdateDTO;
import com.experis.se.mbefj.assignment3.services.genre.GenreService;
import com.experis.se.mbefj.assignment3.services.movie.MovieService;
import com.experis.se.mbefj.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
@RequestMapping("api/v1/genres")
public class GenreController {
    private final GenreService genreService;
    private final MovieService movieService;
    private final GenreMapper genreMapper;

    public GenreController(GenreService genreService, MovieService movieService, GenreMapper genreMapper) {
        this.genreService = genreService;
        this.movieService = movieService;
        this.genreMapper = genreMapper;
    }

    @Operation(summary = "Get all genres.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = GenreDTO.class)))
                    })
    })
    @GetMapping
    public ResponseEntity findAll() {
        var genres = genreService.findAll();
        return ResponseEntity.ok(genreMapper.genreDTOs(genres));
    }

    @Operation(summary = "Get a genre by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GenreDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Genre does not exist with supplied id.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        var genre = genreService.findById(id);
        return ResponseEntity.ok(genreMapper.genreToGenreDTO(genre));
    }

    @Operation(summary = "Add a new genre to the database.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Genre was created",
                    content = {@Content(mediaType = "application/json")}
            )
    })
    @PostMapping
    public ResponseEntity add(@RequestBody GenreNewDTO genreNewDTO) {
        var genre = genreMapper.genreNewDTOtoGenre(genreNewDTO);
        var addedGenre = genreService.add(genre);
        URI uri = URI.create("movies/" + addedGenre.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update a genre's data. ")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Genre was updated",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Genre id in body does not match id in path.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody GenreUpdateDTO genreUpdateDTO, @PathVariable int id) {
        if (genreUpdateDTO.getId() != id) {
            return ResponseEntity.badRequest().build();
        }
        var genre = genreMapper.genreUpdateDTOtoGenre(genreUpdateDTO);
        genreService.update(genre);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a genre by id.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Genre was deleted",
                    content = {@Content(mediaType = "application/json")}
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "The genre with the specified id did not exist.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        var genre = genreService.findById(id);
        movieService.deleteGenreFromMovies(genre);
        genreService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
