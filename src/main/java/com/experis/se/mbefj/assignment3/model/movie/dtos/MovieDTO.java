package com.experis.se.mbefj.assignment3.model.movie.dtos;
import java.util.List;

public class MovieDTO {
        private int id;
        private String title;
        private String trailerUrl;
        private String pictureUrl;
        private int releaseYear;
        private String director;
        private int franchiseId;
        private List<Integer> characterIds;
        private List<Integer> genreIds;

        public int getId() {
                return id;
        }

        public void setId(int id) {
                this.id = id;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public String getTrailerUrl() {
                return trailerUrl;
        }

        public void setTrailerUrl(String trailerUrl) {
                this.trailerUrl = trailerUrl;
        }

        public String getPictureUrl() {
                return pictureUrl;
        }

        public void setPictureUrl(String pictureUrl) {
                this.pictureUrl = pictureUrl;
        }

        public int getReleaseYear() {
                return releaseYear;
        }

        public void setReleaseYear(int releaseYear) {
                this.releaseYear = releaseYear;
        }

        public String getDirector() {
                return director;
        }

        public void setDirector(String director) {
                this.director = director;
        }

        public int getFranchiseId() {
                return franchiseId;
        }

        public void setFranchiseId(int franchiseId) {
                this.franchiseId = franchiseId;
        }

        public List<Integer> getCharacterIds() {
                return characterIds;
        }

        public void setCharacterIds(List<Integer> characterIds) {
                this.characterIds = characterIds;
        }

        public List<Integer> getGenreIds() {
                return genreIds;
        }

        public void setGenreIds(List<Integer> genreIds) {
                this.genreIds = genreIds;
        }
}
