package com.experis.se.mbefj.assignment3.model.genre.dtos;

public class GenreUpdateDTO {
    private int id;
    private String genreName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
