package com.experis.se.mbefj.assignment3.mappers;

import com.experis.se.mbefj.assignment3.model.character.Character;
import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.genre.Genre;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.model.movie.dtos.MovieDTO;
import com.experis.se.mbefj.assignment3.model.movie.dtos.MovieNewDTO;
import com.experis.se.mbefj.assignment3.services.character.CharacterService;
import com.experis.se.mbefj.assignment3.services.franchise.FranchiseService;
import com.experis.se.mbefj.assignment3.services.genre.GenreService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected GenreService genreService;
    @Autowired
    protected CharacterService characterService;

    @Mapping(target = "franchiseId", source = "franchise", qualifiedByName = "franchiseToId")
    @Mapping(target = "characterIds", source = "characters", qualifiedByName = "charactersToIds")
    @Mapping(target = "genreIds", source = "genres", qualifiedByName = "genresToIds")
    public abstract MovieDTO movieToMovieDTO(Movie movies);

    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies);

    @Named("franchiseToId")
    public Integer franchiseToId(Franchise franchise) {
        if (franchise == null) {
            return -1;
        }
        return franchise.getId();
    }

    @Named("charactersToIds")
    public List<Integer> charactersToIds(Collection<Character> characters) {
        if (characters == null) {
            return List.of();
        }

        return characters.stream()
                .map(Character::getId)
                .toList();
    }

    @Named("genresToIds")
    public List<Integer> genresToIds(Collection<Genre> genres) {
        if (genres == null) {
            return List.of();
        }

        return genres.stream()
                .map(Genre::getId)
                .toList();
    }

    public abstract Movie movieNewDTOToMovie(MovieNewDTO movieNewDTO);

    @Mapping(target = "franchise", source = "franchiseId", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "genres", source = "genreIds", qualifiedByName = "genreIdsToGenres")
    @Mapping(target = "characters", source = "characterIds", qualifiedByName = "characterIds")
    public abstract Movie movieDTOtoMovie(MovieDTO movieDTO);

    @Named("franchiseIdToFranchise")
    public Franchise franchiseIdToFranchise(Integer id) {
        return franchiseService.findById(id);
    }

    @Named("genreIdsToGenres")
    public Set<Genre> genreIdsToGenres(List<Integer> genreIds) {
        if (genreIds == null) {
            return Set.of();
        }

        return genreIds.stream()
                .map(genreService::findById)
                .collect(Collectors.toSet());
    }

    @Named("characterIds")
    public Set<Character> characterIds(List<Integer> characterIds) {
        if (characterIds == null) {
            return Set.of();
        }

        return characterIds.stream()
                .map(characterService::findById)
                .collect(Collectors.toSet());
    }
}
