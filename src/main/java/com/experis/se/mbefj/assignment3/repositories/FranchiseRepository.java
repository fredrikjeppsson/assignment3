package com.experis.se.mbefj.assignment3.repositories;

import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {

}
