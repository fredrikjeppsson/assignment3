package com.experis.se.mbefj.assignment3.mappers;

import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.franchise.dtos.FranchiseUpdateDTO;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.model.franchise.dtos.FranchiseDTO;
import com.experis.se.mbefj.assignment3.services.franchise.FranchiseService;
import com.experis.se.mbefj.assignment3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;
    @Autowired
    FranchiseService franchiseService;

    @Mapping(target = "movies", source = "movie", qualifiedByName = "moviesToIds")

    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);

    @Mapping(target = "movie", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchiseDto);

    public abstract Franchise franchiseUpdateDtoToFranchise(FranchiseUpdateDTO franchiseUpdateDTO);

    @Named("moviesToIds")
    Set<Integer> mapMoviesToId(Set<Movie> movie){
        if (movie == null){
            return null;
        }
        return movie.stream()
                .map(Movie::getId).collect(Collectors.toSet());
    }

    @Named("movieIdToMovie")
    Set<Movie> mapIdToMovie(Set<Integer> id){
        if (id == null){
            return null;
        }
        return id.stream()
                .map(movieService::findById)
                .collect(Collectors.toSet());
    }
}
