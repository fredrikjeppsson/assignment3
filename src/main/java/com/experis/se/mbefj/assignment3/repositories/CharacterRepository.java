package com.experis.se.mbefj.assignment3.repositories;

import com.experis.se.mbefj.assignment3.model.character.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {
}
