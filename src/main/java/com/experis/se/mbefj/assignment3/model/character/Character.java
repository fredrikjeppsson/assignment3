package com.experis.se.mbefj.assignment3.model.character;

import com.experis.se.mbefj.assignment3.model.movie.Movie;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String fullName;

    @Column
    private String charAlias;

    @Column
    private String gender;

    @Column(columnDefinition = "TEXT")
    private String pictureUrl;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCharAlias() {
        return charAlias;
    }

    public void setCharAlias(String charAlias) {
        this.charAlias = charAlias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
