package com.experis.se.mbefj.assignment3.model.character.dtos;

import java.util.List;

public class CharacterDTO {
    private int id;
    private String fullName;
    private String charAlias;
    private String gender;
    private String pictureUrl;
    private List<Integer> movieIds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCharAlias() {
        return charAlias;
    }

    public void setCharAlias(String charAlias) {
        this.charAlias = charAlias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<Integer> getMovieIds() {
        return movieIds;
    }

    public void setMovieIds(List<Integer> movieIds) {
        this.movieIds = movieIds;
    }
}
