package com.experis.se.mbefj.assignment3.services.movie;

import com.experis.se.mbefj.assignment3.model.character.Character;
import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.model.genre.Genre;
import com.experis.se.mbefj.assignment3.model.movie.Movie;
import com.experis.se.mbefj.assignment3.services.CrudService;

import java.util.List;

public interface MovieService extends CrudService<Movie, Integer> {
    void updateCharacters(int id, List<Integer> movieCharactersIds);
    List<Movie> findMoviesByFranchise(Franchise franchise);
    void updateMoviesInFranchise(int id, List<Integer> movies);

    void deleteCharacterFromMovies(Character character);

    void deleteGenreFromMovies(Genre genre);

    void deleteFranchiseFromMovies(Franchise franchise);

    void updateGenres(int id, List<Integer> genreIds);
}
