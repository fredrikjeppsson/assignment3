package com.experis.se.mbefj.assignment3.runners;

import com.experis.se.mbefj.assignment3.repositories.GenreRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class GenreRunner implements ApplicationRunner {
    private final GenreRepository genreRepository;

    public GenreRunner(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Transactional
    @Override
    public void run(ApplicationArguments args) throws Exception {
//        System.out.println(genreRepository.findAll());
    }
}
