package com.experis.se.mbefj.assignment3.services.franchise;

import com.experis.se.mbefj.assignment3.model.franchise.Franchise;
import com.experis.se.mbefj.assignment3.services.CrudService;

public interface FranchiseService extends CrudService<Franchise, Integer> {
}
