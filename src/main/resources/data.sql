insert into franchise (franchise_name, description) values
                                                        ('Marvel Cinematic Universe', 'The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics.'),
                                                        ('Pixar', 'Pixar Animation Studios is an American CGI film production company based in Emeryville, California, United States. Pixar has produced 26 feature films, which were all released by Walt Disney Studios Motion Pictures through the Walt Disney Pictures banner, with their first being Toy Story (which was also the first feature-length CGI film ever released) in 1995, and their latest being Lightyear in 2022.'),
                                                        ('Star Wars', 'Star Wars is an American epic space-opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon.');

insert into movie (title, trailer_url, picture_url, release_year, director, franchise_id) values
                                                                                           ('Iron Man',
                                                                                            'https://www.imdb.com/video/vi447873305/?playlistId=tt0371746&ref_=tt_ov_vi',
                                                                                            'https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_FMjpg_UX1000_.jpg',
                                                                                            2008, 'Jon Favreau', 1), -- 1
                                                                                           ('Thor',
                                                                                            'https://www.imdb.com/video/vi1431476761/?playlistId=tt0800369&ref_=tt_pr_ov_vi',
                                                                                            'https://m.media-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_.jpg',
                                                                                            2011, 'Kenneth Branagh', 1), -- 2
                                                                                           ('Iron Man 3',
                                                                                            'https://www.imdb.com/video/vi2830738969/?playlistId=tt1300854&ref_=tt_pr_ov_vi',
                                                                                            'https://m.media-amazon.com/images/M/MV5BMjE5MzcyNjk1M15BMl5BanBnXkFtZTcwMjQ4MjcxOQ@@._V1_.jpg',
                                                                                            2013, 'Shane Black', 1), -- 3
                                                                                           ('Guardians of the Galaxy',
                                                                                            'https://www.imdb.com/video/vi1441049625/?playlistId=tt2015381&ref_=tt_pr_ov_vi',
                                                                                            'https://static.posters.cz/image/1300/poster/guardians-of-the-galaxy-one-sheet-i85524.jpg',
                                                                                            2014, 'James Gunn', 1), -- 4
                                                                                           ('Black Panther',
                                                                                            'https://www.imdb.com/video/vi2320939289?playlistId=tt1825683&ref_=tt_ov_vi',
                                                                                            'https://m.media-amazon.com/images/I/61a03Zq9oRL._AC_.jpg',
                                                                                            2018, 'Ryan Coogler', 1), -- 5
                                                                                           ('Toy Story',
                                                                                            'https://www.imdb.com/video/vi2052129305?playlistId=tt0114709&ref_=tt_ov_vi',
                                                                                            'https://m.media-amazon.com/images/M/MV5BMDU2ZWJlMjktMTRhMy00ZTA5LWEzNDgtYmNmZTEwZTViZWJkXkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_.jpg',
                                                                                            1995, 'John Lasseter', 2),  -- 6
                                                                                           ('Luca',
                                                                                            'https://www.imdb.com/video/vi3001794585?playlistId=tt12801262&ref_=tt_ov_vi',
                                                                                            'https://m.media-amazon.com/images/M/MV5BZTQyNTU0MDktYTFkYi00ZjNhLWE2ODctMzBkM2U1ZTk3YTMzXkEyXkFqcGdeQXVyNTI4MzE4MDU@._V1_.jpg',
                                                                                            2021, 'Enrico Casarosa', 2),  -- 7
                                                                                           ('Star Wars: Episode IV - A New Hope',
                                                                                            'https://www.imdb.com/video/vi1317709849?playlistId=tt0076759&ref_=tt_pr_ov_vi',
                                                                                            'https://static.posters.cz/image/750/poster/star-wars-a-new-hope-one-sheet-i28733.jpg',
                                                                                            1977, 'George Lucas', 3), -- 8
                                                                                           ('Star Wars: Episode V - The Empire Strikes Back',
                                                                                            'https://www.imdb.com/video/vi221753881?playlistId=tt0080684&ref_=tt_pr_ov_vi',
                                                                                            'https://static.posters.cz/image/750/poster/star-wars-a-new-hope-one-sheet-i28733.jpg',
                                                                                            1980, 'Irvin Kershner', 3), -- 9
                                                                                           ('Star Wars: Episode VI - Return of the Jedi',
                                                                                            'https://www.imdb.com/video/vi1702936345?playlistId=tt0086190&ref_=tt_ov_vi',
                                                                                            'https://static.posters.cz/image/1300/poster/star-wars-episode-vi-jedins-aterkomst-i90220.jpg',
                                                                                            1983, 'Richard Marquand', 3); -- 10

insert into genre (genre_name) values
                                   ('action'),     -- 1
                                   ('comedy'),     -- 2
                                   ('drama'),      -- 3
                                   ('fantasy'),    -- 4
                                   ('horror'),     -- 5
                                   ('mystery'),    -- 6
                                   ('romance'),    -- 7
                                   ('thriller'),   -- 8
                                   ('western'),    -- 9
                                   ('superhero'),  -- 10
                                   ('animation'),  -- 11
                                   ('sports'),     -- 12
                                   ('scifi');      -- 13
insert into movie_genres (movies_id, genres_id) values
                                                 (1, 1),     -- Iron Man/Action
                                                 (1, 10),    -- Iron Man/Superhero
                                                 (2, 4),     -- Thor/Fantasy
                                                 (2, 10),    -- Thor/Superhero
                                                 (2, 1),     -- Thor/Action
                                                 (3, 1),     -- Iron Man 3/Action
                                                 (3, 10),    -- Iron Man 3/Superhero
                                                 (4, 1),     -- Guardians/Action
                                                 (4, 10),    -- Guardians/Superhero
                                                 (4, 2),     -- Guardians/Comedy
                                                 (4, 4),     -- Guardians/Fantasy
                                                 (5, 1),     -- Black Panther/Action
                                                 (5, 10),    -- Black Panther/Superhero
                                                 (6, 2),     -- Toy Story/Comedy
                                                 (6, 11),    -- Toy Story/Animation
                                                 (6, 4),     -- Toy Story/Fantasy
                                                 (7, 2),     -- Luca/Comedy
                                                 (7, 11),    -- Luca/Animation
                                                 (7, 1),     -- Star Wars IV/Action
                                                 (8, 13),    -- Star Wars IV/Scifi
                                                 (8, 4),     -- Star Wars IV/Fantasy
                                                 (9, 1),     -- Star Wars V/Action
                                                 (9, 13),    -- Star Wars V/Scifi
                                                 (9, 4),     -- Star Wars V/Fantasy
                                                 (10, 1),     -- Star Wars VI/Action
                                                 (10, 13),    -- Star Wars VI/Scifi
                                                 (10, 4);     -- Star Wars VI/Fantasy

insert into character (full_name, char_alias, gender, picture_url) values
                                                                       ('Tony Stark', 'Iron Man', 'male', 'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/3/35/IronMan-EndgameProfile.jpg/revision/latest/top-crop/width/360/height/360?cb=20190423175213'),           -- 1
                                                                       ('Thor', null, 'male', 'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/2/22/Thor_in_LoveAndThunder_Poster.jpg/revision/latest?cb=20220615195810'),                       -- 2
                                                                       ('Peter Quill', 'Star Lord', 'male', 'https://upload.wikimedia.org/wikipedia/en/b/b2/Chris_Pratt_as_Peter_Quill.jpeg'),         -- 3
                                                                       ('Gamora', null, 'female', 'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/6/61/Gamora_AIW_Profile.jpg/revision/latest?cb=20180518212221'),                   -- 4
                                                                       ('T''Challa', 'Black Panther', 'male', 'https://upload.wikimedia.org/wikipedia/en/1/1a/Chadwick_Boseman_as_T%27Challa.jpg'),       -- 5
                                                                       ('N''Jadaka', 'Killmonger', 'male', 'https://static.wikia.nocookie.net/marveldatabase/images/7/78/N%27Jadaka_%28Earth-199999%29_from_Black_Panther_%28film%29_005.jpg/revision/latest?cb=20180220195031'),          -- 6
                                                                       ('Woody', null, 'toy', 'https://upload.wikimedia.org/wikipedia/en/0/01/Sheriff_Woody.png'),                       -- 7
                                                                       ('Buzz Lightyear', null, 'toy', 'https://upload.wikimedia.org/wikipedia/en/b/b4/Buzz_Lightyear.png'),              -- 8
                                                                       ('Luca Paguro', 'sea monster', 'male', 'https://static.wikia.nocookie.net/disney/images/0/04/Profile_-_Luca_Paguro.jpg/revision/latest?cb=20210720235829'),       -- 9
                                                                       ('Alberto Scorfano', 'sea monster', 'male', 'https://static.wikia.nocookie.net/pixar/images/5/5c/Human_Alberto.png/revision/latest?cb=20210814144019'),  -- 10
                                                                       ('Giulia Marcovaldo', null, 'female', 'https://static.wikia.nocookie.net/pixar/images/5/5c/Giulia_Marcovaldo.png/revision/latest?cb=20210814162726'),        -- 11
                                                                       ('Luke Skywalker', null, 'male', 'https://upload.wikimedia.org/wikipedia/en/9/9b/Luke_Skywalker.png'),             -- 12
                                                                       ('Han Solo', null, 'male', 'https://upload.wikimedia.org/wikipedia/en/b/be/Han_Solo_depicted_in_promotional_image_for_Star_Wars_%281977%29.jpg'),                   -- 13
                                                                       ('Anakin Skywalker', 'Darth Vader', 'male', 'https://static.wikia.nocookie.net/starwars/images/6/6f/Anakin_Skywalker_RotS.png/revision/latest?cb=20130621175844'),  -- 14
                                                                       ('Leia Organa', 'Princess Leia', 'female', 'https://upload.wikimedia.org/wikipedia/en/thumb/1/1b/Princess_Leia%27s_characteristic_hairstyle.jpg/220px-Princess_Leia%27s_characteristic_hairstyle.jpg');   -- 15

insert into movie_characters (movies_id, characters_id) values
                                                         (1, 1),  -- Iron Man/Tony Stark
                                                         (2, 2),  -- Thor/Thor
                                                         (3, 1),  -- Iron Man 3/Tony Stark
                                                         (4, 3),  -- Guardians/Peter Quill
                                                         (4, 4),  -- Guardians/Gamora
                                                         (5, 5),  -- Black Panther/T'Challa
                                                         (5, 6),  -- Black Panther/N'Jadaka
                                                         (6, 7),  -- Toy Story/Woody
                                                         (6, 8),  -- Toy Story/Buzz Lightyear
                                                         (7, 9),  -- Luca/Luca Paguro
                                                         (7, 10), -- Luca/Alberto Scorfano
                                                         (7, 11), -- Guilia Marcavaldo
                                                         (8, 12), -- Star Wars IV/Luke Skywalker
                                                         (8, 13), -- Star Wars IV/Han Solo
                                                         (8, 14), -- Star Wars IV/Anakin Skywalker
                                                         (8, 15), -- Star Wars IV/Leia
                                                         (9, 12), -- Star Wars V/Luke Skywalker
                                                         (9, 13), -- Star Wars V/Han Solo
                                                         (9, 14), -- Star Wars V/Anakin Skywalker
                                                         (9, 15), -- Star Wars V/Leia
                                                         (10, 12), -- Star Wars VI/Luke Skywalker
                                                         (10, 13), -- Star Wars VI/Han Solo
                                                         (10, 14), -- Star Wars VI/Anakin Skywalker
                                                         (10, 15); -- Star Wars VI/Leia