# Assignment Create a Web API and database with Spring
A RESTful web API made with Spring Web and PostgreSQL database.

The database stores information about movies, characters that appear in the movies, different genres and franchises that the movies belong to.
The database has been created and seeded with dummy data using Hibernate.
We make use of data transfer objects to access and add data to the database.

Documentation has been created using Swagger.

This repo has a CI/CD pipeline which builds the application as a Docker artifact and deploys to Heroku.

## Install
Clone repo:
```
https://gitlab.com/fredrikjeppsson/assignment3.git
```
To run locally you need to add your database credentials in the application.properties file:
```bash
spring.datasource.url=jdbc:postgresql://localhost:5432/{database-name}&{username}&{password}
```

## Usage
Run the main method to start the server locally. Gradle will automatically download necessary dependencies.

See available API endpoints:
[Deployed API endpoints](https://fredj-movie-db.herokuapp.com/swagger-ui/index.html#/)


## Authors

- [Martina Blixt Eriksson](@MartinaBlixtEriksson)

- [Fredrik Jeppsson](@fredjepp)
