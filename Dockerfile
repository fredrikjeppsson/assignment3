FROM gradle:jdk17 AS gradle
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM openjdk:17 as runtime
WORKDIR /app

ENV SPRING_PROFILE "prod"
# Using DDL_AUTO "create" and INIT_MODE "always" specifically
# for this assignment to recreate the database because Heroku's
# free tier deletes the data.
ENV DDL_AUTO "create"
ENV INIT_MODE "always"
ENV SHOW_JPA_SQL "false"

COPY --from=gradle /app/build/libs/*.jar /app/app.jar
RUN chown -R 1000:1000 /app
USER 1000:1000
ENTRYPOINT ["java", "-jar", "app.jar"]